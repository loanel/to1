package agh.edu.pl;

public class App 
{
    public static void main( String[] args )
    {
        /// simple creation to show off functionality

        System.out.println("Car salon system" );
        /// adding stub cars to show functionality
        GasolineEngine gasolineOne = new GasolineEngine(10000.0, "Engine for vehicle 1", GasolineTypes.GASOLINE_B);
        Model modelB = new Model(ModelType.TYPE_B, 50000.0, "Model B of vehicle 1");
        /// we suspect option is like a bonus package for a car, for example an insurance package etc.
        Option optionOne = new Option("Option no.1", "Option 1 for a given vehicle", 25000.0);
        /// creating vehicle
        Vehicle vehicleOne = new Vehicle(gasolineOne, modelB);
        vehicleOne.addOption(optionOne);
        System.out.println(vehicleOne.toString());

        /// diesel vehicle
        DieselEngine dieselOne = new DieselEngine(20000.0, "Diesel enigine for vehicle 2", DieselTypes.DIESEL_A);
        Model modelA = new Model(ModelType.TYPE_A, 45000.50, "Model A of vehicle 2");
        Option optionTwo = new Option("Option no.2", "Option 2 for a given vehicle", 10500.0);
        Vehicle vehicleTwo = new Vehicle(dieselOne, modelA);
        vehicleTwo.addOption(optionOne);
        vehicleTwo.addOption(optionTwo);
        System.out.println(vehicleTwo.toString());
        /// testing design
        //GasolineEngine testEngine = vehicleTwo.getEngine();
        //DieselEngine testEngineTwo = vehicleTwo.getEngine();
       ///  testEngineThree = vehicleTwo.getEngine();
       /// System.out.println(testEngineThree.toString());
        /// and this engine lost its type, design doesn't work
    }
}

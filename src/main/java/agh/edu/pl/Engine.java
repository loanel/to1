package agh.edu.pl;

abstract class Engine implements Component {

    private Double price;
    private String description;

    public Engine(Double price, String description) {
        this.price = price;
        this.description = description;
    }

    public double getPrice() {
        return this.price;
    }

    public String getDescription() {
        return this.description;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public void setDescription(String description) {
        this.description = description;
    }
   /// added toString to so we can diffrentiate engines
    public abstract String toString();
}

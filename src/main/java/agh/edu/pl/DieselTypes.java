package agh.edu.pl;

/**
 * Created by macho_000 on 2017-05-21.
 */
public enum DieselTypes {

    DIESEL_A(1.9), DIESEL_B(1.6);

    private double diesel;
    DieselTypes(double dies){
        diesel = dies;
    }
    double showDiesel(){
        return this.diesel;
    }

}

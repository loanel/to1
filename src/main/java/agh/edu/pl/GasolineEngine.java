package agh.edu.pl;

/**
 * Created by macho_000 on 2017-05-21.
 */
public class GasolineEngine extends Engine {
    GasolineTypes type;

    public GasolineEngine(Double price, String description, GasolineTypes type) {
        super(price, description);
        this.type = type;
    }

    public double getGasolineValue() {
        return type.showGasoline();
    }
    @Override
    public String toString() {
        return "GasolineEngine: " +
                "type=" + type.showGasoline() + ", " +
                "description=" + this.getDescription() + ", " +
                "price=" + this.getPrice();
    }
}

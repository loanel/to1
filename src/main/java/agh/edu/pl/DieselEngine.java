package agh.edu.pl;

/**
 * Created by macho_000 on 2017-05-21.
 */
public class DieselEngine extends Engine {

    DieselTypes type;

    public DieselEngine(Double price, String description, DieselTypes type) {
        super(price, description);
        this.type = type;
    }

    public double getDieselValue() {
        return type.showDiesel();
    }

    @Override
    public String toString() {
        return "DieselEngine: " +
                "type=" + type.showDiesel() + ", " +
                "description=" + this.getDescription() + ", " +
                "price=" + this.getPrice();
    }
}

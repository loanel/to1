package agh.edu.pl;

/**
 * Created by macho_000 on 2017-05-21.
 */

/// this is iffy
public enum GasolineTypes {
    /// this should contain gasoline brands, using stub names
    GASOLINE_A(1.0), GASOLINE_B(1.2), GASOLINE_C(1.4), GASOLINE_D(1.6);

    private double gasoline;
    GasolineTypes(double gas){
        gasoline = gas;
    }
    double showGasoline(){
        return this.gasoline;
    }
}

package agh.edu.pl;

/**
 * Created by macho_000 on 2017-05-21.
 */
public class Option implements Component {

    private String name;
    private Double price;
    private String description;

    public Option(String name, String description, Double price) {
        this.name = name;
        this.price = price;
        this.description = description;
    }

    public double getPrice() {
        return this.price;
    }

    public String getDescription() {
        return this.description;
    }

    /// this shouldn't be called toString, rather getName like in other classes, don't change standards suddenly
    public String getName() {
        return this.name;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}

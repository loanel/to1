package agh.edu.pl;

/**
 * Created by macho_000 on 2017-05-21.
 */
public class Model implements Component {

    private ModelType type;
    private Double price;
    private String description;

    public Model(ModelType type, Double price, String description) {
        this.type = type;
        this.price = price;
        this.description = description;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return this.price;
    }

    public String getDescription() {
        return this.description;
    }

    public ModelType getType() {
        return type;
    }
}

package agh.edu.pl;

import java.util.ArrayList;

/**
 * Created by macho_000 on 2017-05-21.
 */
import java.util.ArrayList;

public class Vehicle {

    /// option on schematics was a single variable, since the method is called addOption maybe it was suposed to be a list?
    private ArrayList<Option> options = new ArrayList<Option>();
    private Engine engine;
    private Model model;

    public Vehicle(Engine engine, Model model){
        this.engine = engine;
        this.model = model;
    }

    public ArrayList<Option> getOptions() {
        return options;
    }
    public void addOption(Option option) {
        options.add(option);
    }
    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    /// hard to implement because each class doesn't have its own toString method
    /// we've added a "virtual" method toString to engine, so all engine types can at least return their types as strings
    @Override
    public String toString() {
        String returns = "Vehicle:" +
                "engine= " + engine.toString() + "\n" +
                "model= " + model.getType() + " " + model.getDescription() + ", price:" + model.getPrice() + "\n" +
                "Available options :\n";
        for(Option o: options){
            returns += o.getName() + " " + o.getDescription() + " " + ", price = " + o.getPrice() + "\n";
        }
        return returns;
    }
}

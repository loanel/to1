package agh.edu.pl;

/**
 * Created by macho_000 on 2017-05-21.
 */
public interface Component {

    double getPrice();
    String getDescription();

}
